﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class AttackCone : MonoBehaviour
    {
        //Reference to TurretAI script
        [SerializeField]
        private TurretAI turretAI = default;

        //Variable to set for left or right AttackRangeTrigger
        [SerializeField]
        private bool isLeft = default;

        private void Awake()
        {
            //Setting TurretAi to turretAI script on turret parent object
            turretAI = gameObject.GetComponentInParent<TurretAI>();
        }

        //Called as long as collider stays in Trigger 
        private void OnTriggerStay2D(Collider2D collision)
        {
            //Checks if collider is on object with "PLayer"-tag
            if (collision.CompareTag("Player"))
            {
                //Checking which AttackrangeTrigger it is
                if (isLeft)
                {
                    //Caklling Attack function from TurretAI and set attackRight to false
                    turretAI.Attack(false);
                }
                else
                {
                    //Caklling Attack function from TurretAI and set attackRight to right
                    turretAI.Attack(true);
                }
            }
        }
    }
}

