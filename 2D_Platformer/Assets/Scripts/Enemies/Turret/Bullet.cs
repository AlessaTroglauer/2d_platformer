﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PRTwo
{
    public class Bullet : MonoBehaviour
    {
        //Player reference
        private Player player;
        //Called when entering trigger
        private void OnTriggerEnter2D(Collider2D collision)
        {
            //If object colliding with trigger is not marked as trigger or has "Turret"-tag
            if (collision.isTrigger != true & collision.tag != ("Enemy"))
            {
                //Checking if object bullet is colliding with has "Player"-tag
                if (collision.CompareTag("Player"))
                {
                    //Calling Damage function from player and reduce health by 10 through raising the event
                    player = collision.GetComponent<Player>();
                    Message.Raise(new PlayerTakesDamageEvent());
                    player.dmg = 10;
                }

                //Destroy bullet
                Destroy(gameObject);
            }
        }
    }
}

