﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class TurretAI : MonoBehaviour
    {
        //Health variables
        [SerializeField]
        private int currentHealth = default;
        [SerializeField]
        private int maxHealth = default;

        //Variable for distance between two objects
        [SerializeField]
        private float distance = default;
        //Value needed to wake up turret
        [SerializeField]
        private float wakeRange = default;
        //Value defining how fast turret is shotting
        [SerializeField]
        private float shootIntervall = default;
        //Value for speed of flying bullet
        [SerializeField]
        private float bulletSpeed = default;
        //Value for a cooldown
        [SerializeField]
        private float bulletTimer = default;

        //Variable to set status of turret
        [SerializeField]
        private bool awake = default;

        //Reference to bullet game object
        [SerializeField]
        private GameObject bullet = default;
        //Reference to position of target
        [SerializeField]
        private Transform target = default;
        //Refernce to Animator
        [SerializeField]
        private Animator animator = default;
        //Reference to the points turret is shooting from
        [SerializeField]
        private Transform shootPointLeft = default;
        [SerializeField]
        private Transform shootPointRight = default;

        //Reference to the attached AudioSource component
        [SerializeField]
        private AudioSource audioSource = default;

        //Value for Audioclip we want to play when opening Menu
        [SerializeField]
        private AudioClip takeDamageSound = default;

        private void Awake()
        {
            //Reference to animator on gameObject
            animator = gameObject.GetComponent<Animator>();
        }

        private void Start()
        {
            //Setting current Health to max health when starting game
            currentHealth = maxHealth;
        }

        private void Update()
        {
            //Setting Parameter of Animator
            animator.SetBool("Awake", awake);

            //Calling RangeCheck
            RangeCheck();

            //Destroy turret when health equals 0
            if (currentHealth <= 0)
            {
                Destroy(gameObject);
            }
        }

        //Function to Check if target is close enough to turret
        void RangeCheck()
        {
            //Setting the disctance float to distance between turret and target
            distance = Vector3.Distance(transform.position, target.transform.position);

            //Sets turret to awake if distance bewtween target and turret is smaller than the wakeRange
            if (distance < wakeRange)
            {
                awake = true;
            }
            else
            {
                awake = false;
            }
        }

        //Attack function called from AttackCone Script
        public void Attack(bool attackingRight)
        {
            //Increase bulletTimer by Time 
            bulletTimer += Time.deltaTime;

            //If time from bulletTimer is greater than the shootIntervall set call the follwing 
            if (bulletTimer >= shootIntervall)
            {
                //Setting direction to turret will shoot to and set length of Vector to 1
                Vector2 direction = target.transform.position - transform.position;
                direction.Normalize();

                //If player os to the turrets right
                if (attackingRight)
                {
                    //Instantiate a bulletClone at right shootPoint and let it shoot to direction of palyer with bulletSpeed
                    GameObject bulletClone;
                    bulletClone = Instantiate(bullet, shootPointRight.transform.position, shootPointRight.transform.rotation) as GameObject;
                    bulletClone.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

                    //Reset bulletTimer
                    bulletTimer = 0;
                }
                else
                {
                    //Instantiate a bulletClone at left shootPoint and let it shoot to direction of palyer with bulletSpeed
                    GameObject bulletClone;
                    bulletClone = Instantiate(bullet, shootPointLeft.transform.position, shootPointLeft.transform.rotation) as GameObject;
                    bulletClone.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

                    //Reset bulletTimer
                    bulletTimer = 0;
                }
            }
        }

        //Function to make turret vulnerable
        public void TakeDamage(int dmg)
        {
            audioSource.PlayOneShot(takeDamageSound);

            //Reduce currentHealth of Turret by dmg
            currentHealth -= dmg;
            gameObject.GetComponent<Animation>().Play("Player_RedFlash");
        }
    }
}


