﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class Spikes : MonoBehaviour
    {
        //Reference to player script
        private Player player;

        void Start()
        {
            //Set player variable equal to player script
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }

        //Called when collider enters trigger
        private void OnTriggerEnter2D(Collider2D collision)
        {
            //Check if object colliding has "Player"-tag
            if (collision.CompareTag("Player"))
            {
                //Calling TakeDamage function from Player and set dmg to 15
                Message.Raise(new PlayerTakesDamageEvent());
                player.dmg = 15;

                //Calling IEnueratpr from Player and set variables
                StartCoroutine(player.Knockback(0.01f, 200, player.transform.position));
            }
        }
    }
}


