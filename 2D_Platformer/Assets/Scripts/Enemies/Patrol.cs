﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PRTwo
{
    public class Patrol : MonoBehaviour
    {
        //Health variables
        [SerializeField]
        private int currentHealth = default;

        //Variable to define how fast object moves
        [SerializeField]
        private float moveSpeed = default;

        //Variables to set length of ray
        [SerializeField]
        private float rayDistanceGround = default;
        [SerializeField]
        private float rayDistanceWall = default;

        //Set to true when the Patrol moves right
        [SerializeField]
        private bool movingRight = default;

        //Reference to groundDetection object set in the Inspector
        [SerializeField]
        private Transform groundDetection = default;

        //Reference to Player
        private Player player = default;

        //Reference to the attached AudioSource component
        [SerializeField]
        private AudioSource audioSource = default;

        //Value for Audioclip we want to play when opening Menu
        [SerializeField]
        private AudioClip takeDamageSound = default;

        //Collider to describe the player
        Collider2D playerCollider;

        private void OnEnable()
        {
            //Adds OnEnemyKillEvent as a subscriber to KillEnemyEvent
            Message<KillEnemyEvent>.Add(OnEnemyKillEvent);
        }

        private void OnDisable()
        {
            //Removes OnEnemyKillEvent as a subscriber from KillEnemyEvent
            Message<KillEnemyEvent>.Remove(OnEnemyKillEvent);
        }

        private void Start()
        {
            //Setting player to Playerr Script on Player object
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            //Gets the PlayerAttack component from the parent
            PlayerAttack playerAttack = player.GetComponent<PlayerAttack>();
        }

        private void Update()
        {
            //Move Patrol  
            transform.Translate(Vector2.right * moveSpeed * Time.deltaTime);

            //Creating Raycasts for ground and wall with origin, direction and distance
            RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, rayDistanceGround);
            RaycastHit2D wallInfo = Physics2D.Raycast(groundDetection.position, Vector2.right, rayDistanceWall);

            //Checking if the ray hit wall or no ground
            if (groundInfo.collider == false || wallInfo.collider)
            {
                //Switch moving direction depending on direction before
                if (movingRight)
                {
                    transform.eulerAngles = new Vector3(0, -180, 0);
                    movingRight = false;
                }
                else
                {
                    transform.eulerAngles = new Vector3(0, 0, 0);
                    movingRight = true;
                }
            }

            //Destroy Patrol when health equals 0
            if (currentHealth <= 0)
            {
                //Raises event
                Message.Raise(new KillEnemyEvent());
            }

        }
        //Subscriber that gets executed once the EnemyKillEvent takes place
        void OnEnemyKillEvent(KillEnemyEvent ev)
        {
            //If the current health is less or equal to zero
            if(currentHealth <= 0)
            {
                //Destroys gameObject  this script is attached to
                Destroy(gameObject);
            }
            
        }

        //Called when collider enters trigger
        private void OnTriggerEnter2D(Collider2D collision)
        {
            //Check if object colliding has "Player"-tag
            if (collision.CompareTag("Player"))
            {
                //Sets the collision to the playerCollider
                collision = playerCollider;
                
                //Calling TakeDamage Event from Player and set dmg to 20 
                Message.Raise(new PlayerTakesDamageEvent());
                player.dmg = 20;

                ////Calling IEnueratpr from Player and set variables
                StartCoroutine(player.Knockback(0.02f, 350, player.transform.position));

            }
        }

        //Function called from AttackTrigger to damage patrol
        public void TakeDamage(int damage)
        {
            audioSource.PlayOneShot(takeDamageSound);
            //Reduce currentHealth by damage set in other script
            currentHealth -= damage;

            //Play RedFlash animation
            gameObject.GetComponent<Animation>().Play("Player_RedFlash");
        }
    }

}
