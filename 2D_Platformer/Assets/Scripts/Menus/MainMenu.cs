﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    public class MainMenu : MonoBehaviour
    {
        //Function called from QuitButton
        //Close Program
        public void Quitgame()
        {
            Application.Quit();
        }
    }
}

