﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    public class PauseMenu : MonoBehaviour
    {
        //Sets Game to active or inactive
        [SerializeField]
        private static bool gameIsPaused = default;

        //Reference to pauseMenuUI
        [SerializeField]
        private GameObject pauseMenuUI = default;

        //Reference to the attached AudioSource component
        [SerializeField]
        private AudioSource audioSource = default;

        //Value for Audioclip we want to play when opening Menu
        [SerializeField]
        private AudioClip openPauseMenuSound = default;

        //Calling Methods when escape is pressed
        void Update()
        {
            //Checking if Esc Button is pressed
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                //Calls Resume if Game is already paused or pauses it when its active
                if (gameIsPaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }

        //Function to resume the agme after being paused 
        public void Resume()
        {
            //Disable PauseMenu
            pauseMenuUI.SetActive(false);

            //Set timeScale back to normal
            Time.timeScale = 1f;

            //Set gameisPaused to false
            gameIsPaused = false;
        }

        //Function to ´pause the game
        void Pause()
        {
            //Enable PauseMenu
            pauseMenuUI.SetActive(true);

            //Freeze time
            Time.timeScale = 0f;

            //Set gameisPaused to true
            gameIsPaused = true;

            //Play AudioSource when opening PauseMenu
            audioSource.PlayOneShot(openPauseMenuSound);

        }

        //Method to LoadMainMenu
        public void LoadMenu()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene("MainMenu");
        }

        //Method to Exit the Game
        public void QuitGame()
        {
            Application.Quit();
        }

    }

}
