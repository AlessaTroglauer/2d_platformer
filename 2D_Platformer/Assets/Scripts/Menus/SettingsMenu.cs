﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio; 
using UnityEngine;
using System;

namespace PRTwo
{
    public class SettingsMenu : MonoBehaviour
    {
        //Reference to the attached AudioMixer
        public AudioMixer audioMixer;

        //Gets called when moving slider
        //Sets value of slider to volume
        public void SetVolume(float volume)
        {
            audioMixer.SetFloat("Volume", volume);
        }


    }
}

