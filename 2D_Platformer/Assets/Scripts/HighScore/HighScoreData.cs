﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    //class to hold the highscore data
    [SerializeField]
    public class HighscoreData
    {
        public int highScore;
    }
}
