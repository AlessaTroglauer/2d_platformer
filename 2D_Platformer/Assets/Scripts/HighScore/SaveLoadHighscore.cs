﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;

namespace PRTwo
{
    public class SaveLoadHighscore : MonoBehaviour
    {
        //Int to hold the loaded highscore
        private int loadedHighScore;
        //bool to check if the save exsists
        private bool doesSaveExsist;
        //Reference to our highscore Data
        private HighscoreData highscoreData;
        //String to describe the filepath in which the score is saved
        private string filePath;
        //Int to describe the current in game score
        private int currentScore;
        //reference to the scorecounter
        [SerializeField] private ScoreCounter counter;

        private void OnEnable()
        {
            //Adds subscruber to the corresponding event
            Message<LoadingScoreEvent>.Add(OnLoadScoreEvent);
            Message<SavingScoreEvent>.Add(OnSaveHighscoreEvent);
        }

        private void OnDisable()
        {
            //Removes subscriber from the corresponging event
            Message<LoadingScoreEvent>.Remove(OnLoadScoreEvent);
            Message<SavingScoreEvent>.Remove(OnSaveHighscoreEvent);
        }

        private void Awake()
        {
            //Sets the filepath for the save
            filePath = Application.persistentDataPath + "/Highscore.json";
            //Is the save exsists we load the score
            doesSaveExsist = LoadHighScore();
        }

        private void Update()
        {
            //Updates the current score accordingly 
            currentScore = counter.score;
        }

        //Subscriber that executes once the SaveScoreEvent gets raised
        void OnSaveHighscoreEvent(SavingScoreEvent ev)
        {
            //If our current score is higher than the loaded score
            if (currentScore >= loadedHighScore)
            {
                //We save the highscore
                string filepath = Application.persistentDataPath + "/Highscore.json";
                HighscoreData highscoreData = new HighscoreData();
                highscoreData.highScore = currentScore;
                string dataToSave = JsonUtility.ToJson(highscoreData);
                File.WriteAllText(filepath, dataToSave);
            }
            //And set our bool to true as something was now saved
            doesSaveExsist = true;
        }

        //bool to load the highscore
        bool LoadHighScore()
        {
            //If we have a save in our filepath
            if (File.Exists(filePath))
            {
                //raises the event
                Message.Raise(new LoadingScoreEvent());
                return true;
            }
            //If not return the bool as false
            return false;
        }
        //Subscriber that executes once the events has been raised
        void OnLoadScoreEvent(LoadingScoreEvent ev)
        {
            //Logic to load the highscore from our filepath
            string loadedHighScoreData = File.ReadAllText(filePath);
            highscoreData = JsonUtility.FromJson<HighscoreData>(loadedHighScoreData);
            loadedHighScore = highscoreData.highScore;
        }

        //Property of the current score
        int CurrentScore
        {
            get { return currentScore; }
            set { currentScore = value; }
        }
    }
}