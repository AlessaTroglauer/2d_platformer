﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;


namespace PRTwo
{
    public class DisplayHighScore : MonoBehaviour
    { 
    [SerializeField] private string noScore;
    [SerializeField] private TextMeshProUGUI textMesh;

    private float loadedHighScore;
    private bool doesSaveExsist;

    private HighscoreData highscoreData;

    private string filePath;

    private void Awake()
    {
        filePath = Application.persistentDataPath + "/Highscore.json";

        doesSaveExsist = LoadHighScore();
    }

    void Start()
    {
        if(doesSaveExsist)
        {
            textMesh.text = loadedHighScore.ToString();
        }
        else
        {
            textMesh.text = noScore;
        }
    }

    bool LoadHighScore()
    {
        if (File.Exists(filePath))
        {
            string loadedHighScoreData = File.ReadAllText(filePath);
            highscoreData = JsonUtility.FromJson<HighscoreData>(loadedHighScoreData);
            loadedHighScore = highscoreData.highScore;
            return true;
        }
        return false;
    }
    }

}
