﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

namespace PRTwo
{
    public class Timer : MonoBehaviour
    {
        //Time when timer starts
        private float startTime = default;

        //Reference to timer text in canvas 
        [SerializeField]
        private TextMeshProUGUI timerText = default;

        void Start()
        {
            //Assigning startTime to time since application started
            startTime = Time.time;
        }

        void Update()
        {
            //Time when timer starts
            float t = Time.time - startTime;

            //remove decimals
            //amount of seconds devided by 60
            //Convert float into string 
            string minutes = ((int)t / 60).ToString();
            //Convert float into string
            //Remove decimals
            string seconds = (t % 60).ToString("f0");

            //If length of seconds or minutes is one put extra 0 null in front
            if (seconds.Length == 1)
            {
                seconds = "0" + seconds;
            }
            if (minutes.Length == 1)
            {
                minutes = "0" + minutes;
            }

            //Set timer text equal to passed minutes and seconds 
            timerText.text = minutes + ":" + seconds;
        }
    }

}
