﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PRTwo
{
    public class HealthBar : MonoBehaviour
    {
        //Reference to Slider
        [SerializeField]
        private Slider healthSlider = default;

        //Creting Gradient
        [SerializeField]
        private Gradient gradient = default;

        //Create Reference to fill Image
        [SerializeField]
        private Image fill = default;

        public void SetMaxHealth(int health)
        {
            //Set max Value to health 
            //Making sure slider starts at maximum health 
            healthSlider.maxValue = health;
            healthSlider.value = health;

            //Get Green color from Gradient
            fill.color = gradient.Evaluate(1f);
        }

        public void SetHealth(int health)
        {
            //Set value of Slider equal to health
            healthSlider.value = health;

            //Setting Color of healthSlider
            fill.color = gradient.Evaluate(healthSlider.normalizedValue);
        }
    }
}

