﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

namespace PRTwo
{
    public class ScoreCounter : MonoBehaviour
    {
        //Create value for score
        public int score = default;

        //Reference to score text
        [SerializeField]
        private TextMeshProUGUI scoreText = default;

        //Adding the OnIncreaseScore method to subscribe to the BloodPickUpEvent
        //Adding the OnDoubleScore method to subscribe to the PickUpBloodPowerUpEvent
        private void OnEnable()
        {
            Message<BloodPickUpEvent>.Add(OnIncreaseScore);
            Message<PickUpPowerUpBloodEvent>.Add(OnDoubleScore);
        }

        //Removing the OnIncreaseScore method to unsubscribe from the BloodPickUpEvent
        //Removing the OnDoubleScore method to unsubscribe from the PickUpBloodPowerUpEvent
        private void OnDisable()
        {
            Message<BloodPickUpEvent>.Remove(OnIncreaseScore);
            Message<PickUpPowerUpBloodEvent>.Remove(OnDoubleScore);
        }

        private void Update()
        {
            //Set text to score value
            scoreText.text = ("x " + score);
        }

        //Method that is called when PickUpBloodEvent is called
        private void OnIncreaseScore(BloodPickUpEvent ev)
        {
            //Increase score by one 
            score += 1;
        }

        //Method that is called when PickUpPowerUpBloodEvent is called
        private void OnDoubleScore(PickUpPowerUpBloodEvent ev)
        {
            //Double score
            score *= 2;
        }
    }
}

