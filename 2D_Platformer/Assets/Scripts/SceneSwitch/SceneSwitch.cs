﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    public class SceneSwitch : MonoBehaviour
    {
        //Reference to LevelLoader
        private LevelLoader levelLoader;

        //Reference to the attached AudioSource component
        [SerializeField]
        private AudioSource audioSource = default;

        //Value for Audioclip we want to play when opening Menu
        [SerializeField]
        private AudioClip sceneSwitchSound = default;

        private void Start()
        {
            //Set levelLoader to LevelLoader Script on Crossfade object
            levelLoader = GameObject.FindGameObjectWithTag("Crossfade").GetComponent<LevelLoader>();
        }
        //switches scene on entering collider
        void OnTriggerEnter2D(Collider2D collision)
        {
            //if object colliding has "Player"-tag
            if (collision.CompareTag("Player"))
            {
                //play audio clip once
                audioSource.PlayOneShot(sceneSwitchSound);

                //Call IEnumerator from LevelLoader
                //Load next Level 
                StartCoroutine(levelLoader.LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
            }
        }
    }
}



