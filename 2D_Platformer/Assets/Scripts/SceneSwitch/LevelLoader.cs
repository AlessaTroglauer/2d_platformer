﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using System;

namespace PRTwo
{
    public class LevelLoader : MonoBehaviour
    {
        //Reference to Animator
        [SerializeField]
        private Animator transition = default;

        private void OnEnable()
        {
            //Adds subscriber to the corresponding event
            Message<SceneLoadFadeEvent>.Add(OnLevelLoadingEvent);
        }

        private void OnDisable()
        {
            //Removes subscriber from the corresponding event
            Message<SceneLoadFadeEvent>.Remove(OnLevelLoadingEvent);
        }

        public void LoadNextLevel()
        {
            //Start IEnumerator LoadLevel
            //Load next level in build Index
            StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
        }

        public IEnumerator LoadLevel(int levelIndex)
        {
            //Raises the event
            Message.Raise(new SceneLoadFadeEvent());

            //Wait for one second
            yield return new WaitForSeconds(1);

            //Load Scene 
            SceneManager.LoadScene(levelIndex);
        }
        //Subscriber that executes once the event gets raised
        void OnLevelLoadingEvent(SceneLoadFadeEvent ev)
        {
            Debug.Log("Fading");
            //Start Animation
            transition.SetTrigger("Start");
        }
    }
}

