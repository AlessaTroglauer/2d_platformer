﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using System;

namespace PRTwo
{
    public class SceneSwitchEnding : MonoBehaviour
    {
        //Reference to LevelLoader
        private LevelLoader levelLoader;

        //Reference to the attached AudioSource component
        [SerializeField]
        private AudioSource audioSource = default;

        //Value for Audioclip we want to play when opening Menu
        [SerializeField]
        private AudioClip sceneSwitchSound = default;

        [SerializeField] private SaveLoadHighscore highscore;

        private void Start()
        {
            //Set levelLoader to LevelLoader Script on Crossfade object
            levelLoader = GameObject.FindGameObjectWithTag("Crossfade").GetComponent<LevelLoader>();
            
        }
        //switches scene on entering collider
        void OnTriggerEnter2D(Collider2D collision)
        {
            //if object colliding has "Player"-tag
            if (collision.CompareTag("Player"))
            {
                //play audio clip once
                audioSource.PlayOneShot(sceneSwitchSound);

                //Call IEnumerator from LevelLoader
                //Load Main Menu
                StartCoroutine(levelLoader.LoadLevel(0));

                //If the highscore is not null
                if(highscore != null)
                {
                    //Raises the event
                    Message.Raise(new SavingScoreEvent());
                }
            }
        }
    }
}


