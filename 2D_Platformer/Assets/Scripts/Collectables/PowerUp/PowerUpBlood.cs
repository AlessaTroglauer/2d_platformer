﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class PowerUpBlood : MonoBehaviour
    {
        //Reference to the attached AudioSource component
        [SerializeField]
        private AudioSource audioSource = default;

        //Audioclip to play when opening Menu
        [SerializeField]
        private AudioClip collectingSound = default;

        //Adding the OnPickUpBloodPowerUp method to subscribe to the PickUpBloodPowerUpEvent
        private void OnEnable()
        {
            Message<PickUpPowerUpBloodEvent>.Add(OnPickUpBlooderPowerUp);
        }

        //Removing the OnPickUpBloodPowerUp method to unsubscribe from the PickUpBloodPowerUpEvent
        private void OnDisable()
        {
            Message<PickUpPowerUpBloodEvent>.Remove(OnPickUpBlooderPowerUp);
        }

        //On entering trigger of object
        private void OnTriggerEnter2D(Collider2D other)
        {
            //If object entering has "Player"-tag
            if (other.transform.CompareTag("Player"))
            {
                //Raising the PickUpBloodPowerUpEvent and informing every other object in the world that subscribed 
                Message.Raise(new PickUpPowerUpBloodEvent());

                //Destroy Powerup
                Destroy(gameObject);
            }
        }

        //Method that is called when PickUpPowerUpBloodEvent is called
        private void OnPickUpBlooderPowerUp(PickUpPowerUpBloodEvent ev)
        {
            //Play assigned audioClip once
            audioSource.PlayOneShot(collectingSound);
        }
    }
}

