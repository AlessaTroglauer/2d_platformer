﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class PowerUpHealth : MonoBehaviour
    {
        //Reference to the attached AudioSource component
        [SerializeField]
        private AudioSource audioSource = default;

        //Value for Audioclip to play when opening Menu
        [SerializeField]
        private AudioClip collectingSound = default;

        //Adding the OnPickUpHealthPowerUp method to subscribe to the PickUpHealthPowerUpEvent
        private void OnEnable()
        {
            Message<PickUpPowerUpHealthEvent>.Add(OnPickUpHealthPowerUp);
        }

        //Remove the OnPickUpHealthPowerUp method to unsubscribe from the PickUpHealthPowerUpEvent
        private void OnDisable()
        {
            Message<PickUpPowerUpHealthEvent>.Remove(OnPickUpHealthPowerUp);
        }

        //On entering trigger of object
        private void OnTriggerEnter2D(Collider2D other)
        {
            ////If object entering has "Player"-tag
            if (other.transform.CompareTag("Player"))
            {
                //Raising the PickUpHealthPowerUpEvent and informing every other object in the world that subscribed 
                Message.Raise(new PickUpPowerUpHealthEvent());

                //Destroy PowerUp
                Destroy(gameObject);
            }
        }

        //Method that is called when PickUpPowerUpHealthEvent is called
        private void OnPickUpHealthPowerUp(PickUpPowerUpHealthEvent ev)
        {
            //Play assigned audioClip once
            audioSource.PlayOneShot(collectingSound);
        }
    }
}

