﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class PowerUpSpeed : MonoBehaviour
    {
        //Extra speed player will walk with
        [SerializeField]
        public float extraSpeed;

        //Reference to the attached AudioSource component
        [SerializeField]
        private AudioSource audioSource = default;

        //Value for Audioclip to play when opening Menu
        [SerializeField]
        private AudioClip collectingSound = default;

        //Set if hasActivated 
        public bool hasActivated = default;

        //Adding the OnPickUpPowerUpSpeed method to subscribe to the PickUpPowerUpSpeedEvent
        private void OnEnable()
        {
            Message<PickUpPowerUpSpeedEvent>.Add(OnPickUpPowerUpSpeed);
        }

        //Removing the OnPickUpPowerUpSpeed method to unsubscribe from the PickUpPowerUpSpeedEvent
        private void OnDisable()
        {
            Message<PickUpPowerUpSpeedEvent>.Remove(OnPickUpPowerUpSpeed);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            //If object entering trigger has "Player"-tag
            if (other.transform.CompareTag("Player"))
            {
                //Call IEnumerator
                StartCoroutine(PickUp());

                //Raising the PickUpBloodPowerUpEvent and informing every other object in the world that subscribed 
                Message.Raise(new PickUpPowerUpSpeedEvent());


            }
        }

        IEnumerator PickUp()
        {
            //Set hasActivated to true
            hasActivated = true;
            
            //Activate Function for 10 seconds
            yield return new WaitForSeconds(10);

            //Set hasActivated to false
            hasActivated = false;

            //Reference to PlayerMovement Script on Player
            PlayerMovement newSpeed = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
            
            //Set moveSpeed back to normal
            newSpeed.moveSpeed -= extraSpeed;

            //Destroy PowerUp
            Destroy(gameObject);
        }

        //Method that is called when PickUpPowerUpSpeedEventis called
        void OnPickUpPowerUpSpeed(PickUpPowerUpSpeedEvent ev)
        {
            if (hasActivated == true)
            {
                //Play assigned audioClip once
                audioSource.PlayOneShot(collectingSound);

                //Reference to PlayerMovement Script on Player
                PlayerMovement newSpeed = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();

                //Add extraSpeed to moveSpeed
                newSpeed.moveSpeed += extraSpeed;

                //Disable SpriteRenderer and BoxCollider on PowerUp
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
            }
        }
    }
}

