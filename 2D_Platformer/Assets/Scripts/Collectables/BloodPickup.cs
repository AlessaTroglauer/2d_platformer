﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace PRTwo
{

    public class BloodPickup : MonoBehaviour
    {
        //Variable for creating reference to scoreCounter 
        private ScoreCounter scoreCounter;

        //Reference to the attached AudioSource component
        [SerializeField]
        private AudioSource audioSource = default;

        //Value for Audioclip to play when opening Menu
        [SerializeField]
        private AudioClip collectingSound = default;

        //Adding the OnPickUpBlood method to subscribe to the BloodPickUpEvent
        private void OnEnable()
        {
            Message<BloodPickUpEvent>.Add(OnPickUpBlood);
        }

        //Removing the OnPickUpBlood method to unsubscribe from the BloodPickUpEvent
        private void OnDisable()
        {
            Message<BloodPickUpEvent>.Remove(OnPickUpBlood);
        }

        ////Called when sth enters trigger
        private void OnTriggerEnter2D(Collider2D other)
        {
            //If colliding object has "Player"-tag
            if (other.transform.CompareTag("Player"))
            {
                //Raising the BloodPickUpEvent and informing every other object in the world that subscribed 
                Message.Raise(new BloodPickUpEvent());

                //Destroy the Blood
                Destroy(gameObject);
            }
        }

        //Method that is called when PickUpBloodEvent is called 
        private void OnPickUpBlood(BloodPickUpEvent ev)
        {
            //Play attached audioClip once 
            audioSource.PlayOneShot(collectingSound);
        }
    }
}

