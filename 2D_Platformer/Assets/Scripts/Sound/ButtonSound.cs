﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PRTwo
{
    public class ButtonSound : MonoBehaviour
    {
        //Reference to the attached AudioSource component
        public AudioSource audioSource;

        //Value for Audioclip we want to play when hovering over
        public AudioClip hoverSound;

        //Value for Audioclip we want to play when clicking
        public AudioClip clickSound;

        //Adding the OnHorizonttalInput method to subscribe to the HorizontalInputEvent
        private void OnEnable()
        {
            Message<OnButtonClickEvent>.Add(OnButtonClick);
            Message<OnButtonHoverEvent>.Add(OnButtonHover);
        }

        //Remove the OnHorizonttalInput method to unsubscribe from the HorizontalInputEvent
        private void OnDisable()
        {
            Message<OnButtonClickEvent>.Remove(OnButtonClick);
            Message<OnButtonHoverEvent>.Remove(OnButtonHover);
        }

        //Method for sound Hovering set in in Event Trigger 
        //Plays assigned AudioClip once
        public void HoverSound()
        {
            Message.Raise(new OnButtonHoverEvent());
        }

        void OnButtonHover(OnButtonHoverEvent ev)
        {
            audioSource.PlayOneShot(hoverSound);
        }
        //Method for sound Clicking set in in Event Trigger 
        //Plays assigned AudioClip once
        public void ClickSound()
        {
            Message.Raise(new OnButtonClickEvent());
        }

        void OnButtonClick(OnButtonClickEvent ev)
        {
            audioSource.PlayOneShot(clickSound);
        }
    }
}

