﻿using UnityEngine;
using UnityEditor;
using System;

namespace PRTwo
{
    // The main message class is generic, so we can pass in generic classes or structs
    // The main message class is generic, so we can pass in generic classes or structs
    public class Message<T>
    {
        // public static instance of the Message Object itself, so we can access it from everywhere 
        // and don't need to make a variable out of it
        public static readonly Message<T> Instance = new Message<T>();

        // a generic Action as delegate so we can use generic callbacks
        private Action<T> delegateAction = delegate { };

        /// <summary>
        /// Internal Set method to set a callback handler
        /// </summary>
        /// <param name="handler"></param>
        private void InternalSet(Action<T> handler)
        {
            delegateAction = handler;
        }

        /// <summary>
        /// The public static Set Method, which calls our InternalSet Method
        /// This method will be used from "outside" to set events
        /// </summary>
        /// <param name="handler"></param>
        public static void Set(Action<T> handler)
        {
            Instance.InternalSet(handler);
        }

        /// <summary>
        /// Our internal add method to subscribe to the generic callback
        /// </summary>
        /// <param name="handler"></param>
        private void InternalAdd(Action<T> handler)
        {
            delegateAction += handler;
        }

        /// <summary>
        /// The public static Add Method, which calls our InternalAdd Method
        /// This method will be used from "outside" to subscribe to events
        /// </summary>
        /// <param name="handler"></param>
        public static void Add(Action<T> handler)
        {
            Instance.InternalAdd(handler);
        }

        /// <summary>
        /// Our internal remove method to unsubscribe from a generic callback
        /// </summary>
        /// <param name="handler"></param>
        private void InternalRemove(Action<T> handler)
        {
            delegateAction -= handler;
        }


        /// <summary>
        /// The public static Remove Method, which calls our InternalRemove Method
        /// This method will be used from "outside" to unsubscribe from events
        /// </summary>
        /// <param name="handler"></param>
        public static void Remove(Action<T> handler)
        {
            Instance.InternalRemove(handler);
        }

        /// <summary>
        /// Our internal raise method to fire/post/send a generic event
        /// </summary>
        /// <param name="genericEvent"></param>
        private void InternalRaise(T genericEvent)
        {
            delegateAction(genericEvent);
        }

        /// <summary>
        /// The public static Raise method, which calls our InternalRaise Method
        /// This method will be used from "outside" to fire events
        /// </summary>
        /// <param name="genericEvent"></param>
        public static void Raise(T genericEvent)
        {
            Instance.InternalRaise(genericEvent);
        }
    }

    /// <summary>
    /// This static class is for pure convenience
    /// </summary>
    public static class Message
    {
        /// <summary>
        /// This static method is also is for pure convenience
        /// We won't to fire events quickly, so we create and use this Helper method, so we can write for example:
        /// 
        /// Message.Raise(new GameLoadedEvent());  <--- clean and shorter
        ///
        /// instead of
        ///
        /// Message<GameLoadedEvent>.Raise(new GameLoadedEvent());   <--- longer and unnecessary information
        /// 
        /// </summary>
        /// <param name="genericEvent"></param>
        /// <typeparam name="T"></typeparam>
        public static void Raise<T>(T genericEvent)
        {
            Message<T>.Raise(genericEvent);
        }
    }
}
