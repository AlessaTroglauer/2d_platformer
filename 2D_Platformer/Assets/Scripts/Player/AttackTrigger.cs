﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PRTwo
{
    public class AttackTrigger : MonoBehaviour
    {
        //Value for damage player will create
        [SerializeField]
        private int dmg = default;
        //Collider of our enemies
        Collider2D enemy;

        private void OnEnable()
        {
            //Adds subscriber to the corresponding event
            Message<MouseClickAttackEvent>.Add(OnMouseClickAttackEvent);
        }

        private void OnDisable()
        {
            //Removes subscriber from the corresponding event
            Message<MouseClickAttackEvent>.Remove(OnMouseClickAttackEvent);
        }

        private void Start()
        {
            //Gets the PlayerAttack component from the parent
            PlayerAttack playerAttack = GetComponentInParent<PlayerAttack>();
        }
        //Called when Entering collider
        private void OnTriggerEnter2D(Collider2D collision)
        {
            //Checking if colliding object has "Enemy"-tag
            if (collision.CompareTag("Enemy"))
            {
                //sets our enemy collider2D to the collider we entered
                enemy = collision;
            }

        }

        //Subscriber that gets executed once the event got raised
        void OnMouseClickAttackEvent(MouseClickAttackEvent ev)
        {
            //Checks if we collided yet
            if (enemy != null)
            {
                //Enemy takes damage when we press the mouse and are in its collider
                enemy.SendMessageUpwards("TakeDamage", dmg);
            }

        }
    }
}

