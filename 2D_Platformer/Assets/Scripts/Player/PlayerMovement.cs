﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PRTwo
{
    public class PlayerMovement : MonoBehaviour
    {
        //Variable to set speed of Player in Inspector
        public float moveSpeed = default;

        //Variable to set jump Height of Player in Inspector
        [SerializeField]
        private float jumpForce = default;

        //Reference to Rigidbody2D
        private Rigidbody2D rb2d;

        //Reference to animator set in the Inspector
        [SerializeField]
        private Animator animator = default;


        private void Start()
        {
            //Setting rb2d to Rigidbody from gameObject
            rb2d = gameObject.GetComponent<Rigidbody2D>();

            //Setting animator to Animator from gameObject
            animator = gameObject.GetComponent<Animator>();

        }

        //Adding the OnHorizonttalInput method to subscribe to the HorizontalInputEvent
        private void OnEnable()
        {
            Message<HorizontalInputEvent>.Add(OnHorizonttalInput);
            
            Message<OnSpacePressedEvent>.Add(OnSpacePressed);
        }

        //Remove the OnHorizonttalInput method to unsubscribe from the HorizontalInputEvent
        private void OnDisable()
        {
            Message<HorizontalInputEvent>.Remove(OnHorizonttalInput);
            
            Message<OnSpacePressedEvent>.Remove(OnSpacePressed);
        }


        void Update()
        {
            // checks the Horizontal Input and activates the given event if its positiv
            if ((Input.GetAxis("Horizontal") != 0))
            {
                //Raising the HorizontalInputEvent and informing every other object in the world that subscribed
                Message.Raise(new HorizontalInputEvent());
            }

            // checks the spacekey and activates the given event if its positiv
            if (Input.GetButtonDown("Jump"))
            {
                Message.Raise(new OnSpacePressedEvent());
            }

            //Call SetAnimationState function
            SetAnimationState();
        }

        void OnSpacePressed(OnSpacePressedEvent ev)
        {
            //accelerates the Player Model on the y axis
            rb2d.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
        }

        //Method that is called when HorizontalInputEvent is called
        private void OnHorizonttalInput(HorizontalInputEvent ev)
        {
            //Set movement to Horizontal Input
            Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f);

            //Move player
            transform.position += movement * Time.deltaTime * moveSpeed;

            //Turn player depending on Input
            if (Input.GetAxis("Horizontal") < -0.1f)
            {
                transform.localScale = new Vector3(-1, 1, 1);
            }
            else
            {
                transform.localScale = new Vector3(1, 1, 1);
            }
        }

        private void SetAnimationState()
        {
            //Set Move or Idle Animation
            animator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));

            if (rb2d.velocity.y == 0)
            {
                animator.SetBool("isFlying", false);
                animator.SetBool("isFalling", false);
            }

            //Set Falling Animation
            if (rb2d.velocity.y < 0)
            {
                animator.SetBool("isFalling", true);
                animator.SetBool("isFlying", false);
            }

            //Set Flying Animation 
            if (rb2d.velocity.y > 0)
            {
                animator.SetBool("isFlying", true);
            }
        }
    }
}

