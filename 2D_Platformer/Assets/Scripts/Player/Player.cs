﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement; 
using UnityEngine;

namespace PRTwo
{
    public class Player : MonoBehaviour
    {
        //Health of Player
        public int currentHealth = default;

        //Maximal Heaalth of Player
        [SerializeField]
        private int maxHealth = default;

        [SerializeField]
        private int extraHealth = default;

        //Refernce to HealthBar
        [SerializeField]
        private HealthBar healthbar = default;

        //Reference to the attached AudioSource component
        [SerializeField]
        private AudioSource audioSource = default;

        //Value for Audioclip we want to play when opening Menu
        [SerializeField]
        private AudioClip dyingSound = default;

        //Value for Audioclip we want to play when opening Menu
        [SerializeField]
        private AudioClip takeDamageSound = default;

        public int dmg;

        private void Start()
        {
            //Set currentHealth to maxHealth when starting Level
            currentHealth = maxHealth;

            //Set Healthbar to MaxHealth
            healthbar.SetMaxHealth(maxHealth);
        }

        //Adding the OnRegenerateHealth method to subscribe to the PickUpHealthPowerUpEvent
        private void OnEnable()
        {
            Message<PickUpPowerUpHealthEvent>.Add(OnRegenerateHealth);
            Message<PlayerTakesDamageEvent>.Add(PlayerTakesDamage);
            Message<PlayerDeathEvent>.Add(PlayerDeath);
        }

        //Remove the OnRegenerateHealth method to unsubscribe from the PickUpHealthPowerUpEvent
        private void OnDisable()
        {
            Message<PickUpPowerUpHealthEvent>.Remove(OnRegenerateHealth);
            Message<PlayerTakesDamageEvent>.Remove(PlayerTakesDamage);
            Message<PlayerDeathEvent>.Remove(PlayerDeath);
        }

        private void Update()
        {
            //Check if currentHealth is greater than maxHealth
            if (currentHealth > maxHealth)
            {
                //Set currentHealth to maxHealth
                currentHealth = maxHealth;
            }

            //Check if currentHealth is smaller or equal to zero
            if (currentHealth <= 0)
            {
                Message.Raise(new PlayerDeathEvent());
            }

            //Set value of of health bar based on currentHealth
            healthbar.SetHealth(currentHealth);
        }

        void TakeDamage()
        {
            Message.Raise(new PlayerTakesDamageEvent());
        }

        //Function to reduce player health
        public void PlayerTakesDamage(PlayerTakesDamageEvent ev)
        {
            audioSource.PlayOneShot(takeDamageSound);
            //Reduce player health by value of dmg
            currentHealth -= dmg;

            //Play "Player_RedFlash" Animation
            gameObject.GetComponent<Animation>().Play("Player_RedFlash");
        }

        private void PlayerDeath(PlayerDeathEvent ev)
        {
            audioSource.PlayOneShot(dyingSound);
            //Load Scene which was active before dying
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        //Function to push player back
        public IEnumerator Knockback(float knockbackDuration, float knockbackPower, Vector2 knockbackDirection)
        {
            //timer variable to count the time later on
            float timer = 0;

            //Executing while knockbackDuration is greater than timer set
            while (knockbackDuration > timer)
            {
                //Increasing timer in seconds
                timer += Time.deltaTime;

                //Adding force to Rigidbody of Player
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(transform.position.x, knockbackDirection.y + knockbackPower));
            }

            //Stop IEnumerator 
            yield return 0;
        }

        //Method that is called when PickUpPowerUpHealthEvent is called
        private void OnRegenerateHealth(PickUpPowerUpHealthEvent ev)
        {
            //Adding extraHealth to currentHealth
            currentHealth += extraHealth;        
        }
    }
}

