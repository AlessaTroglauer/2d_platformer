﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PRTwo
{
    public class PlayerAttack : MonoBehaviour
    {
        //Setting if player is attacking
        private bool attacking = default;

        //Variables for cooldown
        private float attackTimer = default;
        [SerializeField]
        private float attackCoolDown = default;

        //Reference to AttackTrigger
        [SerializeField]
        private Collider2D attackTrigger = default;

        //Rference to Animator
        private Animator animator = default;

        //Reference to the attached AudioSource component
        [SerializeField]
        private AudioSource audioSource = default;

        //Value for Audioclip we want to play when opening Menu
        [SerializeField]
        private AudioClip attackingSound = default;

        private void OnEnable()
        {
            //Adds subscriber to the corresponding event
            Message<MouseClickAttackEvent>.Add(OnMouseClickEvent);
        }

        private void OnDisable()
        {
            //Removes subscriber from the corresponding event
            Message<MouseClickAttackEvent>.Remove(OnMouseClickEvent);
        }

        private void Awake()
        {
            //Creating reference to Animator on Player
            animator = gameObject.GetComponent<Animator>();

            //Disabling attackTrigger
            attackTrigger.enabled = false;
        }

        private void Update()
        {
            // checks the mousekey and activates the given event if its positiv
            if (Input.GetMouseButtonDown(0) && !attacking)
            {
                //Raises the event
                Message.Raise(new MouseClickAttackEvent());
            }

            //If player is attacking 
            if (attacking)
            {
                //Check if attackTimer is greater than null
                if (attackTimer > 0)
                {
                    //Decrease attackTimer over time
                    attackTimer -= Time.deltaTime;
                }
                else
                {
                    //Set attacking to false
                    attacking = false;

                    //Disable attackTrigger
                    attackTrigger.enabled = false;
                }

                //Calling animation
                animator.SetBool("Attacking", attacking);

            }

        }

        //Subscriber that gets executed once the event is raised
        void OnMouseClickEvent(MouseClickAttackEvent ev)
        {
            //Plays the attacking sound
            audioSource.PlayOneShot(attackingSound);

            //Set attacking to true
            attacking = true;

            //Set attackTimer to attackCoolDown
            attackTimer = attackCoolDown;

            //Activate attackTrigger
            attackTrigger.enabled = true;

        }
    }
}

