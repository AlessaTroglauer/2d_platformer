﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class CameraFollow : MonoBehaviour
    {
        //Reference to what the camerra is following
        [SerializeField]
        private Transform target = default;

        //Offset of camera to create distance
        [SerializeField]
        private Vector3 offset = default;

        private void LateUpdate()
        {
            //Set position of camera to position of target plus the offset defined 
            transform.position = target.position + offset;
        }
    }
}

