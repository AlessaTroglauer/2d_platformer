﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class Parallax : MonoBehaviour
    {
        //Length of Sprite
        private float length = default;

        //Start position of Sprite
        private float startPosition = default;

        //Reference to camera
        [SerializeField]
        private GameObject cam = default;

        //Value for how much effect will be applied
        [SerializeField]
        private float parallaxEffect = default;

        void Start()
        {
            //Set startPosition to x value of the object the script is sitting on
            startPosition = transform.position.x;

            //Find the size of the length of the Sprites
            length = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        }

        void Update()
        {
            //Variable defining how far it has moved in world space
            float distance = (cam.transform.position.x * parallaxEffect);

            //Moving Sprites
            transform.position = new Vector3(startPosition + distance, transform.position.y, transform.position.z);

            //How far sprite was moved relative to the camera
            float temp = (cam.transform.position.x * (1 - parallaxEffect));

            //Repeating of Background
            if (temp > startPosition + length)
            {
                startPosition += length;
            }
            else if (temp < startPosition - length)
            {
                startPosition -= length;
            }
        }
    }
}

